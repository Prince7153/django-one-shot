from django.forms import ModelForm
from todos.models import TodoList, TodoItem


class ToDoList_form(ModelForm):
    class Meta:
        model = TodoList
        fields = ["name"]


class ToDoItem_form(ModelForm):
    class Meta:
        model = TodoItem
        fields = ["task", "due_date", "is_completed", "list"]
